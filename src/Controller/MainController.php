<?php

namespace App\Controller;

use App\Entity\Products;
use App\Repository\CategoriesRepository;
use App\Repository\ProductsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(CategoriesRepository $CategoriesRepository , ProductsRepository $productsRepository ): Response
    {


        return $this->render('main/index.html.twig',[
            'categorie' => $CategoriesRepository->findBy([], ['categoryOrder' => 'asc']),
            'products'=> $productsRepository->findAll()
        ]);
    }


}
