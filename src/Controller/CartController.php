<?php

namespace App\Controller;

use App\Service\cart\CartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;



#[Route('/cart', name: 'cart_')]
class CartController extends AbstractController
{

    #[Route('/', name: 'panier')]
    public  function index( CartService $cartService )
    {
        return $this->render('cart/index.html.twig', [
            'items' => $cartService->getFullcart(),
            'total' => $cartService->getTotal(),
        ]);
    }


    #[Route('/add/{id}', name: 'add_cart')]
    public function add($id, CartService $cartService)
    {
         $cartService->addItems($id);
        return $this->redirectToRoute('app_home');

    }


    #[Route('/cart/remove/{id}', name: 'remove')]
    public function remove( $id, CartService $cartService)
    {
        $cartService->removeItems($id);

        return $this->redirectToRoute('cart_panier');

    }


}