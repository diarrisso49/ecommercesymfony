<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\RegistrationFormType;
use App\Repository\UsersRepository;
use App\Security\UsersAuthenticator;
use App\Service\JWTService;
use App\Service\SendMailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationController extends AbstractController
{
    #[Route('/register', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, UserAuthenticatorInterface $userAuthenticator, UsersAuthenticator $authenticator, EntityManagerInterface $entityManager, SendMailerService $mail, JWTService $jwt): Response
    {
        $user = new Users();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            //on creer le Header 
            $header = [
                'typ' => 'JWT',
                'alg' =>  'HS256'
            ];

            // on crée le payload

            $payoad = [
                'user_id' => $user->getId()
            ];
            // on crée le tokem 

            $token = $jwt->generate($header,$payoad, $this->getParameter('app.jwtSecret') );
            //dd($token);

            // do anything else you need here, like send an email
             
            $mail->send(
                "no-replay@gmail.com", 
                $user->getEmail(), 
                "Activation compte email",
                'register',
                compact('user', 'token')
            );

            return $userAuthenticator->authenticateUser(
                $user,
                $authenticator,
                $request
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route('/verif/{token}', name: 'verify_user')]
    public function verifyUser($token, JWTService $jwt, UsersRepository $usersRepository, EntityManagerInterface $em): Response
    {
        //On vérifie si le token est valide, n'a pas expiré et n'a pas été modifié
        if($jwt->isValid($token) && !$jwt->isExpired($token) && $jwt->check($token, $this->getParameter('app.jwtSecret'))){
            // On récupère le payload
            $payload = $jwt->getPayload($token);

            // On récupère le user du token
            $user = $usersRepository->find($payload['user_id']);

            //On vérifie que l'utilisateur existe et n'a pas encore activé son compte
            if($user && !$user->getIsVerified()){
                $user->setIsVerified(true);
                $em->flush($user);
                $this->addFlash('success', 'User is activated');
                return $this->redirectToRoute('profile_index');
            }
        }
        // Ici un problème se pose dans le token
        $this->addFlash('danger', 'The token is invalid or has expired');
        return $this->redirectToRoute('app_login');
    }


    #[Route('/renvoiverif', name: 'resend_verif')]
    public function resendVerif(JWTService $jwt, SendMailerService $email, UsersRepository $userpository):Response
    {
        $user = $this->getUser();
       if (!$user) {
           $this->addFlash('danger', 'You should be logged in to access this platform');
           return $this->redirectToRoute('app_login');
       }
       if($user->getIsVerified())
       {
           $this->addFlash('warming', 'this user is already activated');
           return  $this->redirectToRoute('profile_index');
       }

        //on creer le Header
        $header = [
            'typ' => 'JWT',
            'alg' => 'HS256'
        ];

        // on crée le payload

        $payoad = [
            'user_id' => $user->getId()
        ];
        // on crée le tokem

        $token = $jwt->generate($header, $payoad, $this->getParameter('app.jwtSecret'));
        // do anything else you need here, like send an email

        $email->send(
            'no-replay@gmail.com',
            $user->getEmail(),
            'Activation compte email',
            'register',
            compact('user', 'token')
        );
        $this->addFlash('success', 'verification email is sent to your email');
        return $this->redirectToRoute('profile_index');
    }
}   

 