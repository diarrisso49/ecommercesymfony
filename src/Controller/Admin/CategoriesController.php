<?php

namespace App\Controller\Admin;

use App\Entity\Categories;
use App\Entity\Images;
use App\Form\CategoriesFormType;
use App\Form\CategoryFormType;
use App\Form\ProductFormType;
use App\Repository\CategoriesRepository;
use App\Service\PictureService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/admin/categories', name:'categories_admin_')]
class CategoriesController extends AbstractController
{

    #[Route('/', name:'index')]
    public function index(CategoriesRepository $categoriesRepository):Response
    {
        $categories = $categoriesRepository->findBy([], ['categoryOrder' => 'asc']);

        return $this->render('admin/categorie/categorie.html.twig', compact('categories'));
    }


    #[Route('/create', name:'create')]
    public function create(Request $request, EntityManagerInterface $entityManager,
                           SluggerInterface $slugger,)
    {

        $this->denyAccessUnlessGranted('ROLE_USER');


        $categories = new Categories();
        $form = $this->createForm(CategoriesFormType::class, $categories);

        $form->handleRequest($request);

        if ($form->isSubmitted() &&  $form->isValid()) {

            dd($categories);
            die();
            $entityManager->persist($categories);

            $entityManager->flush();

            $this->addFlash('success', 'The Categories has been added');
            return  $this->redirectToRoute('categories_admin_create');

        }

        return $this->render('admin/categorie/add.html.twig', [
            'form' => $form->createView(),
        ]);

     }



}