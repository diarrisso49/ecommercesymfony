<?php

namespace App\Service\cart;

use App\Repository\ProductsRepository;
use Symfony\Component\HttpFoundation\RequestStack;

class CartService
{
    protected $session;
    protected $productsRepository;

    /**
     * @param RequestStack $requestStac
     * @param ProductsRepository $productsRepository
     */
    public function __construct(private RequestStack $requestStac, ProductsRepository $productsRepository )
    {
        $this->session = $this->requestStac->getSession();
        $this->productsRepository = $productsRepository;
    }

    /**
     * @param int $id
     * @return void
     */
    public function addItems( int $id)
    {
        $panier  = $this->session->get('panier', []);

        if (!empty($panier[$id]))
        {
            $panier[$id] ++;
        }
        else
        {
            $panier[$id] = 1;
        }

        $this->session->set('panier', $panier);

    }

    /**
     * @param int $id
     * @return void
     */
    public function removeItems(int $id)
    {
        $panier = $this->session->get('panier',[]);

        if (!empty($panier[$id])) {
            unset($panier[$id]);
        }

        $this->session->set('panier',$panier);

    }

    /**
     * @return array
     */
    public function getFullcart() : array {

        $panier = $this->session->get('panier', []);



        $cartData = array();


        foreach ($panier as $id => $quqntity)
        {
            $cartData[] =  [
                'products' => $this->productsRepository->find($id),
                'quantity' => $quqntity
            ];
        }


        return  $cartData;
    }

    /**
     * @return float
     */
    public function getTotal(): float {


        $total = 0;
        foreach ($this->getFullcart() as $items)
        {

            $total += $items['products']->getPrice() * $items['quantity'];;
        }

        return $total;

    }
}