<?php

namespace App\DataFixtures;

use App\Entity\Categories;
use Symfony\Component\String\Slugger\SluggerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoriesFixtures extends Fixture
{


   private $slugger;
   private $counter = 1;

   public function __construct( SluggerInterface $slugger)
   {
      
    $this->slugger = $slugger;
    
   }



    public function load(ObjectManager $manager): void
    {

    
        $parent = $this->creatCategories('Computer & Büro', null, $manager);
        
        $this->creatCategories('Notebooks', $parent, $manager);
        $this->creatCategories('MacBook Air', $parent, $manager);
        $this->creatCategories('Batterie', $parent, $manager);

        $parent = $this->creatCategories('Sport & Freizeit', null, $manager);

        $this->creatCategories('E-Scoter', $parent, $manager);
        $this->creatCategories('SKG', $parent, $manager);
        $this->creatCategories('Fitnes-Gerät', $parent, $manager);

        $manager->flush();
    }

    public function creatCategories($name, Categories $parent = null, ObjectManager $manager )
    {
        $categorie = new Categories();
        $categorie->setName($name);
        $categorie->setSlug($this->slugger->slug($categorie->getName())->lower());
        $categorie->setParent($parent);
        $manager->persist(    $categorie);
        $this->addReference('cat_'.$this->counter,     $categorie);
        $this->counter++;

        return     $categorie;
    }
}
