<?php

namespace Container7aBjbla;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_AdsphmXService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.adsphmX' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.adsphmX'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'cartService' => ['privates', 'App\\Service\\cart\\CartService', 'getCartServiceService', true],
        ], [
            'cartService' => 'App\\Service\\cart\\CartService',
        ]);
    }
}
