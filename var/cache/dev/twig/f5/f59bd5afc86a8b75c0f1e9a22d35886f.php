<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/adminNavbar.html.twig */
class __TwigTemplate_8931475144c2c41912d0897dbff9d0a9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/adminNavbar.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/adminNavbar.html.twig"));

        // line 1
        echo "
    <aside class=\"col-3 border border-light bg-white p-2 mt-5 mb-5\">

        <article class=\"card mb-3\">
            <div class=\"card-header\">
                Category
            </div>
            <div class=\"card-body\">
                <p><a href=\"";
        // line 9
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("categories_admin_index");
        echo "\">Categories List</a></p>
                <p><a href=\"";
        // line 10
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("categories_admin_create");
        echo "\">Add Categories</a></p>
            </div>
        </article>
        <article class=\"card mb-3\">
            <div class=\"card-header\">
                Products
            </div>
            <div class=\"card-body\">
                <p><a href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_products_index");
        echo "\">Products List</a></p>
                <p><a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_products_create_");
        echo "\">Add a Product</a></p>
            </div>
        </article>

        <article class=\"card mb-3\">
            <div class=\"card-header\">
              Users
            </div>
            <div class=\"card-body\">
                <p><a href=\"";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_users_index");
        echo "\">List of Users</a></p>
            </div>
        </article>

        <article class=\"card mb-3\">
            <div class=\"card-header\">
                Orders
            </div>
            <div class=\"card-body\">
                <p><a href=\"#\">All Orders</a></p>
            </div>
        </article>


    </aside>";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "_partials/adminNavbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 28,  72 => 19,  68 => 18,  57 => 10,  53 => 9,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
    <aside class=\"col-3 border border-light bg-white p-2 mt-5 mb-5\">

        <article class=\"card mb-3\">
            <div class=\"card-header\">
                Category
            </div>
            <div class=\"card-body\">
                <p><a href=\"{{ path('categories_admin_index') }}\">Categories List</a></p>
                <p><a href=\"{{ path('categories_admin_create') }}\">Add Categories</a></p>
            </div>
        </article>
        <article class=\"card mb-3\">
            <div class=\"card-header\">
                Products
            </div>
            <div class=\"card-body\">
                <p><a href=\"{{ path('admin_products_index') }}\">Products List</a></p>
                <p><a href=\"{{ path('admin_products_create_') }}\">Add a Product</a></p>
            </div>
        </article>

        <article class=\"card mb-3\">
            <div class=\"card-header\">
              Users
            </div>
            <div class=\"card-body\">
                <p><a href=\"{{ path('admin_users_index') }}\">List of Users</a></p>
            </div>
        </article>

        <article class=\"card mb-3\">
            <div class=\"card-header\">
                Orders
            </div>
            <div class=\"card-body\">
                <p><a href=\"#\">All Orders</a></p>
            </div>
        </article>


    </aside>", "_partials/adminNavbar.html.twig", "/var/www/html/templates/_partials/adminNavbar.html.twig");
    }
}
