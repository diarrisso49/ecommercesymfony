<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* products/details.html.twig */
class __TwigTemplate_dfeac234f0b6d600193662dbe1167bc1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "products/details.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "products/details.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "products/details.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <main class=\"container mt-5 mb-3\" id=\"section\">
        <!-- Product section-->
        <section class=\"py-5\">
            <h1 class=\"text-center\">Category ";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["products"]) || array_key_exists("products", $context) ? $context["products"] : (function () { throw new RuntimeError('Variable "products" does not exist.', 9, $this->source); })()), "categories", [], "any", false, false, false, 9), "name", [], "any", false, false, false, 9), "html", null, true);
        echo " </h1>
            <div class=\"container px-4 px-lg-5 my-5\">

                <div class=\"row gx-4 gx-lg-5 align-items-center\">
                    <div class=\"col-md-6\">
                        <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
                            <div class=\"carousel-indicators\">
                                ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["products"]) || array_key_exists("products", $context) ? $context["products"] : (function () { throw new RuntimeError('Variable "products" does not exist.', 16, $this->source); })()), "images", [], "any", false, false, false, 16))));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["iteration"]) {
            // line 17
            echo "                                    <button type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide-to=\"";
            echo twig_escape_filter($this->env, ($context["iteration"] - 1), "html", null, true);
            echo "\" ";
            echo ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 17)) ? ("class=\"active\" aria-current=\"true\"") : (""));
            echo " aria-label=\"Slide ";
            echo twig_escape_filter($this->env, $context["iteration"], "html", null, true);
            echo "\"></button>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['iteration'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "                            </div>
                            <div class=\"carousel-inner\">
                                ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["products"]) || array_key_exists("products", $context) ? $context["products"] : (function () { throw new RuntimeError('Variable "products" does not exist.', 21, $this->source); })()), "images", [], "any", false, false, false, 21));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 22
            echo "                                    <div class=\"carousel-item ";
            echo ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 22)) ? ("active") : (""));
            echo "\">
                                        <img src=\"";
            // line 23
            echo twig_escape_filter($this->env, ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/uploads/Products/mini/300x300-") . twig_get_attribute($this->env, $this->source, $context["image"], "name", [], "any", false, false, false, 23)), "html", null, true);
            echo "\" class=\"d-block w-100\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "name", [], "any", false, false, false, 23), "html", null, true);
            echo "\">
                                    </div>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "                            </div>
                            <button class=\"carousel-control-prev\" type=\"button\" data-target=\"#carouselExampleIndicators\" data-slide=\"prev\">
                                <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                                <span class=\"sr-only\">Previous</span>
                            </button>
                            <button class=\"carousel-control-next\" type=\"button\" data-target=\"#carouselExampleIndicators\" data-slide=\"next\">
                                <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                                <span class=\"sr-only\">Next</span>
                            </button>
                        </div>
                       ";
        // line 39
        echo "                    </div>
                    <div class=\"col-md-6\">

                        <div class=\"small mb-1\">SKU: BST-498</div>
                        <h1 class=\"display-5 fw-bolder\">";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["products"]) || array_key_exists("products", $context) ? $context["products"] : (function () { throw new RuntimeError('Variable "products" does not exist.', 43, $this->source); })()), "name", [], "any", false, false, false, 43), "html", null, true);
        echo "</h1>
                        <div class=\"fs-5 mb-5\">
                            <span class=\"\"> ";
        // line 45
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["products"]) || array_key_exists("products", $context) ? $context["products"] : (function () { throw new RuntimeError('Variable "products" does not exist.', 45, $this->source); })()), "price", [], "any", false, false, false, 45), 2, ",", ""), "html", null, true);
        echo " €</span>
                        </div>
                        <p class=\"lead\">";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["products"]) || array_key_exists("products", $context) ? $context["products"] : (function () { throw new RuntimeError('Variable "products" does not exist.', 47, $this->source); })()), "description", [], "any", false, false, false, 47), "html", null, true);
        echo "</p>

                        <div class=\"d-flex\">
                            ";
        // line 50
        if ((twig_get_attribute($this->env, $this->source, (isset($context["products"]) || array_key_exists("products", $context) ? $context["products"] : (function () { throw new RuntimeError('Variable "products" does not exist.', 50, $this->source); })()), "stock", [], "any", false, false, false, 50) > 0)) {
            // line 51
            echo "                                <form class=\"mt-4 p-4 bg-light\" method=\"post\">
                                    <div class=\"form-group\">
                                        <input class=\"form-control text-center me-3\" id=\"inputQuantity\" type=\"num\" value=\"1\" style=\"max-width: 3rem\" />
                                    </div>
                                    <a class=\"btn btn-outline-dark flex-shrink-0\" href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cart_add_cart", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["products"]) || array_key_exists("products", $context) ? $context["products"] : (function () { throw new RuntimeError('Variable "products" does not exist.', 55, $this->source); })()), "id", [], "any", false, false, false, 55)]), "html", null, true);
            echo "\">
                                        <i class=\"bi-cart-fill me-1\"></i>
                                        Add to cart
                                    </a>
                                </form>
                            ";
        } else {
            // line 61
            echo "                                <p class=\"text-danger\">Temporarily out of stock</p>
                            ";
        }
        // line 63
        echo "                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Related items section-->
        <section class=\"py-5 bg-light\">
            <div class=\"container px-4 px-lg-5 mt-5\">
                <h2 class=\"fw-bolder mb-4\">Related products</h2>
                <div class=\"row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center\">
                    ";
        // line 73
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) || array_key_exists("products", $context) ? $context["products"] : (function () { throw new RuntimeError('Variable "products" does not exist.', 73, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 74
            echo "                    <div class=\"col mb-5\">
                        <div class=\"card h-100\">
                            <!-- Product image-->
                            <img class=\"card-img-top\" src=\"";
            // line 77
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("assets/uploads/Products/mini/300x300-" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "images", [], "any", false, false, false, 77), 0, [], "array", false, false, false, 77), "name", [], "any", false, false, false, 77))), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "images", [], "any", false, false, false, 77), 0, [], "array", false, false, false, 77), "name", [], "any", false, false, false, 77), "html", null, true);
            echo "\"
                            />
                            <!-- Product details-->
                            <div class=\"card-body p-4\">
                                <div class=\"text-center\">
                                    <!-- Product name-->
                                    <h5 class=\"fw-bolder\">";
            // line 83
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 83), "html", null, true);
            echo "</h5>
                                    <!-- Product price-->
                                    ";
            // line 85
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 85), 2, ",", ""), "html", null, true);
            echo " €
                                </div>
                            </div>
                            <!-- Product actions-->
                            <div class=\"card-footer p-4 pt-0 border-top-0 bg-transparent\">
                                <div class=\"text-center\"><a class=\"btn btn-outline-dark mt-auto\" href=\"";
            // line 90
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_products_details", ["slug" => twig_get_attribute($this->env, $this->source, $context["product"], "slug", [], "any", false, false, false, 90)]), "html", null, true);
            echo "\">View options</a></div>
                            </div>
                        </div>
                    </div>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 96
        echo "
                </div>
            </div>
        </section>
    </main>

        ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "products/details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  298 => 96,  286 => 90,  278 => 85,  273 => 83,  262 => 77,  257 => 74,  253 => 73,  241 => 63,  237 => 61,  228 => 55,  222 => 51,  220 => 50,  214 => 47,  209 => 45,  204 => 43,  198 => 39,  186 => 26,  167 => 23,  162 => 22,  145 => 21,  141 => 19,  120 => 17,  103 => 16,  93 => 9,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %} {% endblock %}

{% block body %}
    <main class=\"container mt-5 mb-3\" id=\"section\">
        <!-- Product section-->
        <section class=\"py-5\">
            <h1 class=\"text-center\">Category {{ products.categories.name}} </h1>
            <div class=\"container px-4 px-lg-5 my-5\">

                <div class=\"row gx-4 gx-lg-5 align-items-center\">
                    <div class=\"col-md-6\">
                        <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
                            <div class=\"carousel-indicators\">
                                {% for iteration in 1..products.images|length %}
                                    <button type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide-to=\"{{ iteration - 1 }}\" {{ (loop.first ) ?'class=\"active\" aria-current=\"true\"' : '' }} aria-label=\"Slide {{ iteration }}\"></button>
                                {% endfor %}
                            </div>
                            <div class=\"carousel-inner\">
                                {% for image in products.images %}
                                    <div class=\"carousel-item {{ (loop.first ) ? 'active' : '' }}\">
                                        <img src=\"{{ asset('assets/uploads/Products/mini/300x300-') ~  image.name }}\" class=\"d-block w-100\" alt=\"{{ image.name }}\">
                                    </div>
                                {% endfor %}
                            </div>
                            <button class=\"carousel-control-prev\" type=\"button\" data-target=\"#carouselExampleIndicators\" data-slide=\"prev\">
                                <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                                <span class=\"sr-only\">Previous</span>
                            </button>
                            <button class=\"carousel-control-next\" type=\"button\" data-target=\"#carouselExampleIndicators\" data-slide=\"next\">
                                <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                                <span class=\"sr-only\">Next</span>
                            </button>
                        </div>
                       {# <img class=\"card-img-top mb-5 mb-md-0\"
                             src=\"{{ asset('assets/uploads/Products/mini/300x300-' ~  products.images[0].name) }}\" alt=\"{{ products.images[0].name }}\"
                        />#}
                    </div>
                    <div class=\"col-md-6\">

                        <div class=\"small mb-1\">SKU: BST-498</div>
                        <h1 class=\"display-5 fw-bolder\">{{ products.name }}</h1>
                        <div class=\"fs-5 mb-5\">
                            <span class=\"\"> {{ products.price|number_format(2,',','')  }} €</span>
                        </div>
                        <p class=\"lead\">{{ products.description }}</p>

                        <div class=\"d-flex\">
                            {% if products.stock > 0 %}
                                <form class=\"mt-4 p-4 bg-light\" method=\"post\">
                                    <div class=\"form-group\">
                                        <input class=\"form-control text-center me-3\" id=\"inputQuantity\" type=\"num\" value=\"1\" style=\"max-width: 3rem\" />
                                    </div>
                                    <a class=\"btn btn-outline-dark flex-shrink-0\" href=\"{{ path('cart_add_cart', {id: products.id}) }}\">
                                        <i class=\"bi-cart-fill me-1\"></i>
                                        Add to cart
                                    </a>
                                </form>
                            {%  else %}
                                <p class=\"text-danger\">Temporarily out of stock</p>
                            {% endif %}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Related items section-->
        <section class=\"py-5 bg-light\">
            <div class=\"container px-4 px-lg-5 mt-5\">
                <h2 class=\"fw-bolder mb-4\">Related products</h2>
                <div class=\"row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center\">
                    {% for product in products %}
                    <div class=\"col mb-5\">
                        <div class=\"card h-100\">
                            <!-- Product image-->
                            <img class=\"card-img-top\" src=\"{{ asset('assets/uploads/Products/mini/300x300-' ~  product.images[0].name) }}\" alt=\"{{ product.images[0].name }}\"
                            />
                            <!-- Product details-->
                            <div class=\"card-body p-4\">
                                <div class=\"text-center\">
                                    <!-- Product name-->
                                    <h5 class=\"fw-bolder\">{{ product.name }}</h5>
                                    <!-- Product price-->
                                    {{ product.price|number_format(2,',','')  }} €
                                </div>
                            </div>
                            <!-- Product actions-->
                            <div class=\"card-footer p-4 pt-0 border-top-0 bg-transparent\">
                                <div class=\"text-center\"><a class=\"btn btn-outline-dark mt-auto\" href=\"{{ path('app_products_details', {\"slug\": product.slug} ) }}\">View options</a></div>
                            </div>
                        </div>
                    </div>

                    {% endfor %}

                </div>
            </div>
        </section>
    </main>

        {% endblock %}
{#  carousel avec les images #}
        {#<section class=\"row justify-content-center \">
            <div class=\"col-12\">
                <h1 class=\"text-center\">  {{ products.name }}</h1>
            </div>
            <div class=\"col-6\">
                <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
                    <div class=\"carousel-indicators\">
                        {% for iteration in 1..products.images|length %}
                            <button type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide-to=\"{{ iteration - 1 }}\" {{ (loop.first ) ?'class=\"active\" aria-current=\"true\"' : '' }} aria-label=\"Slide {{ iteration }}\"></button>
                        {% endfor %}
                    </div>
                    <div class=\"carousel-inner\">
                        {% for image in products.images %}
                            <div class=\"carousel-item {{ (loop.first ) ? 'active' : '' }}\">
                            <img src=\"{{ asset('assets/uploads/Products/mini/300x300-') ~  image.name }}\" class=\"d-block w-100\" alt=\"{{ image.name }}\">
                        </div>
                        {% endfor %}
                    </div>
                    <button class=\"carousel-control-prev\" type=\"button\" data-target=\"#carouselExampleIndicators\" data-slide=\"prev\">
                        <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                        <span class=\"sr-only\">Previous</span>
                    </button>
                    <button class=\"carousel-control-next\" type=\"button\" data-target=\"#carouselExampleIndicators\" data-slide=\"next\">
                        <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                        <span class=\"sr-only\">Next</span>
                    </button>
                </div>
            </div>
            <div class=\"col-6\">
                <p>{{ products.description }}</p>
                <p> categories {{ products.categories.name}}</p>
                <p> prix {{ products.price / 100 }} €</p>
                {% if products.stock > 0 %}
                    <a href=\"#\" class=\"btn btn-success\">Add to Cart</a>
                {%  else %}
                    <p class=\"text-danger\">Temporarily out of stock</p>
                {% endif %}
            </div>
        </section>
    </main>

#}


", "products/details.html.twig", "/var/www/html/templates/products/details.html.twig");
    }
}
