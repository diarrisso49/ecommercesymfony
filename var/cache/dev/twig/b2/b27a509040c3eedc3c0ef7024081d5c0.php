<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/users/index.html.twig */
class __TwigTemplate_257f130c1397f9b42e133c318c564063 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/users/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/users/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "admin/users/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo " Users ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"container\">
        <div class=\"row\">
                ";
        // line 8
        $this->loadTemplate("_partials/adminNavbar.html.twig", "admin/users/index.html.twig", 8)->display($context);
        // line 9
        echo "            <main class=\"col-9 border border-light bg-white p-2\">
                <h1>Users</h1>
                <table class=\"table\">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Firstname</th>
                        <th>Lasname</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Isverified</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 24, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 25
            echo "                        <tr>
                            <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 26), "html", null, true);
            echo "</td>
                            <td>";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "lastname", [], "any", false, false, false, 27), "html", null, true);
            echo "</td>
                            <td>";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "firstname", [], "any", false, false, false, 28), "html", null, true);
            echo "</td>
                            <td>";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, false, 29), "html", null, true);
            echo "</td>
                            <td>
                                ";
            // line 31
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["user"], "roles", [], "any", false, false, false, 31));
            foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                // line 32
                echo "
                                  ";
                // line 33
                if (($context["role"] == "ROLE_ADMIN")) {
                    // line 34
                    echo "                                    ";
                    $context["name"] = "Administrateur";
                    // line 35
                    echo "                                    ";
                    $context["color"] = "danger";
                    // line 36
                    echo "                                  ";
                } elseif (($context["role"] == "ROLE_PRODUCT_ADMIN")) {
                    // line 37
                    echo "                                    ";
                    $context["name"] = "Admin Product";
                    // line 38
                    echo "                                    ";
                    $context["color"] = "warming";
                    // line 39
                    echo "                                  ";
                } else {
                    // line 40
                    echo "                                     ";
                    $context["name"] = "Membre";
                    // line 41
                    echo "                                      ";
                    $context["color"] = "success";
                    // line 42
                    echo "                                  ";
                }
                // line 43
                echo "                                 <span class=\"badge bg-";
                echo twig_escape_filter($this->env, (isset($context["color"]) || array_key_exists("color", $context) ? $context["color"] : (function () { throw new RuntimeError('Variable "color" does not exist.', 43, $this->source); })()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["name"]) || array_key_exists("name", $context) ? $context["name"] : (function () { throw new RuntimeError('Variable "name" does not exist.', 43, $this->source); })()), "html", null, true);
                echo "</span>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "                            </td>
                            <td>
                            <div class=\"form-check form-switch\">
                                    <input class=\"form-check-input\" type=\"checkbox\" role=\"switch\" id=\"switch";
            // line 48
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 48), "html", null, true);
            echo "\"  ";
            echo ((twig_get_attribute($this->env, $this->source, $context["user"], "isverified", [], "any", false, false, false, 48)) ? ("checked") : (""));
            echo " disabled>
                                    <label class=\"form-check-label\" for=\"switch";
            // line 49
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 49), "html", null, true);
            echo "\"></label>
                                </div>
                               </td>
                            <td> <a href=\"\" class=\"btn btn-success\">Update</a> </td>
                        </tr>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "
                    </tbody>
                </table>
            </main>
        </div>
    </div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "admin/users/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 56,  193 => 49,  187 => 48,  182 => 45,  171 => 43,  168 => 42,  165 => 41,  162 => 40,  159 => 39,  156 => 38,  153 => 37,  150 => 36,  147 => 35,  144 => 34,  142 => 33,  139 => 32,  135 => 31,  130 => 29,  126 => 28,  122 => 27,  118 => 26,  115 => 25,  111 => 24,  94 => 9,  92 => 8,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %} Users {% endblock %}

{% block body %}
    <div class=\"container\">
        <div class=\"row\">
                {% include '_partials/adminNavbar.html.twig' %}
            <main class=\"col-9 border border-light bg-white p-2\">
                <h1>Users</h1>
                <table class=\"table\">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Firstname</th>
                        <th>Lasname</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Isverified</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% for user in users %}
                        <tr>
                            <td>{{ user.id }}</td>
                            <td>{{ user.lastname }}</td>
                            <td>{{ user.firstname }}</td>
                            <td>{{ user.email }}</td>
                            <td>
                                {%  for role in user.roles %}

                                  {% if role == \"ROLE_ADMIN\" %}
                                    {% set name = \"Administrateur\" %}
                                    {% set color = \"danger\" %}
                                  {% elseif role == \"ROLE_PRODUCT_ADMIN\" %}
                                    {% set name = \"Admin Product\" %}
                                    {% set color = \"warming\" %}
                                  {% else %}
                                     {% set name = \"Membre\" %}
                                      {% set color = \"success\" %}
                                  {% endif %}
                                 <span class=\"badge bg-{{ color }}\">{{ name }}</span>
                                {% endfor %}
                            </td>
                            <td>
                            <div class=\"form-check form-switch\">
                                    <input class=\"form-check-input\" type=\"checkbox\" role=\"switch\" id=\"switch{{ user.id }}\"  {{ user.isverified ? \"checked\" : \"\" }} disabled>
                                    <label class=\"form-check-label\" for=\"switch{{ user.id }}\"></label>
                                </div>
                               </td>
                            <td> <a href=\"\" class=\"btn btn-success\">Update</a> </td>
                        </tr>

                    {% endfor %}

                    </tbody>
                </table>
            </main>
        </div>
    </div>
{% endblock %}
", "admin/users/index.html.twig", "/var/www/html/templates/admin/users/index.html.twig");
    }
}
