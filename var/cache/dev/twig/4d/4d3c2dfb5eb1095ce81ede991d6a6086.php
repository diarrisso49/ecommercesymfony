<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/_paginator.html.twig */
class __TwigTemplate_decdde3534689e05a6d14f6fb7b80791 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/_paginator.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/_paginator.html.twig"));

        // line 1
        if (((isset($context["pages"]) || array_key_exists("pages", $context) ? $context["pages"] : (function () { throw new RuntimeError('Variable "pages" does not exist.', 1, $this->source); })()) > 1)) {
            // line 2
            echo "
    <nav aria-label=\"Page navigation example\">
        <ul class=\"pagination\">
            ";
            // line 5
            if (((isset($context["currentPage"]) || array_key_exists("currentPage", $context) ? $context["currentPage"] : (function () { throw new RuntimeError('Variable "currentPage" does not exist.', 5, $this->source); })()) > 1)) {
                // line 6
                echo "            <li class=\"page-item\">
                <a class=\"page-link\"
                   href=\"";
                // line 8
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 8, $this->source); })()), ["slug" => (isset($context["slug"]) || array_key_exists("slug", $context) ? $context["slug"] : (function () { throw new RuntimeError('Variable "slug" does not exist.', 8, $this->source); })()), "page" => ((isset($context["currentPage"]) || array_key_exists("currentPage", $context) ? $context["currentPage"] : (function () { throw new RuntimeError('Variable "currentPage" does not exist.', 8, $this->source); })()) - 1)]), "html", null, true);
                echo "\">
                    Previous
                </a>
            </li>
            ";
            } else {
                // line 13
                echo "            <li class=\"page-item disabled\">
                <a class=\"page-link\" >Previous</a>
            </li>

            ";
                // line 17
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["pages"]) || array_key_exists("pages", $context) ? $context["pages"] : (function () { throw new RuntimeError('Variable "pages" does not exist.', 17, $this->source); })())));
                foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                    // line 18
                    echo "                <li class=\"page-item ";
                    echo ((($context["page"] == (isset($context["currentPage"]) || array_key_exists("currentPage", $context) ? $context["currentPage"] : (function () { throw new RuntimeError('Variable "currentPage" does not exist.', 18, $this->source); })()))) ? ("active") : (""));
                    echo "\">
                    <a class=\"page-link\"
                       href=\"";
                    // line 20
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 20, $this->source); })()), ["slug" => (isset($context["slug"]) || array_key_exists("slug", $context) ? $context["slug"] : (function () { throw new RuntimeError('Variable "slug" does not exist.', 20, $this->source); })()), "page" => $context["page"]]), "html", null, true);
                    echo "\">
                        ";
                    // line 21
                    echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                    echo "
                    </a>
                </li>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 25
                echo "             ";
                if (((isset($context["currentPage"]) || array_key_exists("currentPage", $context) ? $context["currentPage"] : (function () { throw new RuntimeError('Variable "currentPage" does not exist.', 25, $this->source); })()) < (isset($context["pages"]) || array_key_exists("pages", $context) ? $context["pages"] : (function () { throw new RuntimeError('Variable "pages" does not exist.', 25, $this->source); })()))) {
                    // line 26
                    echo "                 <li class=\"page-item\">
                    <a class=\"page-link\"
                       href=\"";
                    // line 28
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 28, $this->source); })()), ["slug" => (isset($context["slug"]) || array_key_exists("slug", $context) ? $context["slug"] : (function () { throw new RuntimeError('Variable "slug" does not exist.', 28, $this->source); })()), "page" => ((isset($context["currentPage"]) || array_key_exists("currentPage", $context) ? $context["currentPage"] : (function () { throw new RuntimeError('Variable "currentPage" does not exist.', 28, $this->source); })()) + 1)]), "html", null, true);
                    echo "\">
                        next
                    </a>
                 </li>
             ";
                } else {
                    // line 33
                    echo "                 <li class=\"page-item disabled\">
                     <a class=\"page-link\" >next</a>
                 </li>
             ";
                }
                // line 37
                echo "        </ul>
        ";
            }
            // line 39
            echo "    </nav>
";
        }
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "_partials/_paginator.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 39,  115 => 37,  109 => 33,  101 => 28,  97 => 26,  94 => 25,  84 => 21,  80 => 20,  74 => 18,  70 => 17,  64 => 13,  56 => 8,  52 => 6,  50 => 5,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if pages > 1 %}

    <nav aria-label=\"Page navigation example\">
        <ul class=\"pagination\">
            {% if currentPage > 1 %}
            <li class=\"page-item\">
                <a class=\"page-link\"
                   href=\"{{ path(path, {slug:slug, page: currentPage - 1}) }}\">
                    Previous
                </a>
            </li>
            {% else %}
            <li class=\"page-item disabled\">
                <a class=\"page-link\" >Previous</a>
            </li>

            {% for page in 1..pages %}
                <li class=\"page-item {{ (page == currentPage) ? 'active': ''}}\">
                    <a class=\"page-link\"
                       href=\"{{ path(path, {slug:slug, page: page}) }}\">
                        {{ page }}
                    </a>
                </li>
            {% endfor %}
             {% if currentPage < pages  %}
                 <li class=\"page-item\">
                    <a class=\"page-link\"
                       href=\"{{ path(path, {slug:slug, page: currentPage + 1}) }}\">
                        next
                    </a>
                 </li>
             {% else %}
                 <li class=\"page-item disabled\">
                     <a class=\"page-link\" >next</a>
                 </li>
             {% endif %}
        </ul>
        {% endif %}
    </nav>
{% endif %}", "_partials/_paginator.html.twig", "/var/www/html/templates/_partials/_paginator.html.twig");
    }
}
