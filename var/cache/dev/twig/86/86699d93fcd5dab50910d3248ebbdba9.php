<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* cart/index.html.twig */
class __TwigTemplate_3d20251a5ca313b03b7a96653cb9a8a9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "cart/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "cart/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "cart/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo " la liste des products ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
    <section id=\"section\" class=\"my-5 mx-4 h-100\" id=\"section\">
        ";
        // line 7
        if ((twig_length_filter($this->env, (isset($context["items"]) || array_key_exists("items", $context) ? $context["items"] : (function () { throw new RuntimeError('Variable "items" does not exist.', 7, $this->source); })())) > 0)) {
            // line 8
            echo "        <div class=\"container h-100 py-5\">
            <div class=\"row d-flex justify-content-center align-items-center h-100\">
                <div class=\"col-8\">

                    <div class=\"d-flex justify-content-between align-items-center mb-4\">
                        <h3 class=\"fw-normal mb-0 text-black\">Shopping Cart</h3>
                        <div>
                           ";
            // line 17
            echo "                            <h3 class=\"\">
                                <a href=\"";
            // line 18
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_home");
            echo "\" class=\"text-decoration-none fw-normal mb-0 text-white btn btn-primary\">
                                     Add More Products to cart
                                </a>
                            </h3>
                        </div>
                    </div>

                    <div class=\"card rounded-3 mb-4\">
                        ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["items"]) || array_key_exists("items", $context) ? $context["items"] : (function () { throw new RuntimeError('Variable "items" does not exist.', 26, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 27
                echo "                        <div class=\"card-body p-4\">
                            <div class=\"row d-flex justify-content-between align-items-center\">
                                <div class=\"col-md-2 col-lg-2 col-xl-2\">
                                    <img
                                            src=\"";
                // line 31
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("assets/uploads/Products/mini/300x300-" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "products", [], "any", false, false, false, 31), "images", [], "any", false, false, false, 31), 0, [], "array", false, false, false, 31), "name", [], "any", false, false, false, 31))), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "products", [], "any", false, false, false, 31), "images", [], "any", false, false, false, 31), 0, [], "array", false, false, false, 31), "name", [], "any", false, false, false, 31), "html", null, true);
                echo "\"
                                            class=\"img-fluid rounded-3\" >
                                </div>
                                <div class=\"col-md-3 col-lg-3 col-xl-3\">
                                    <p class=\"lead fw-normal mb-2\">";
                // line 35
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "products", [], "any", false, false, false, 35), "name", [], "any", false, false, false, 35), "html", null, true);
                echo "</p>
                                   <!-- <p><span class=\"text-muted\">Size: </span>M <span class=\"text-muted\">Color: </span>Grey</p>!-->
                                </div>
                                <div class=\"col-md-3 col-lg-3 col-xl-2 d-flex\">
                                    <button class=\"btn btn-link px-2\"
                                            onclick=\"this.parentNode.querySelector('input[type=number]').stepDown()\">
                                        <i class=\"bi bi-dash\"></i>
                                    </button>

                                    <input id=\"form1\" min=\"0\" name=\"quantity\" value=\"";
                // line 44
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 44), "html", null, true);
                echo "\" type=\"number\"
                                           class=\"form-control form-control-sm\" />

                                    <button class=\"btn btn-link px-2\"
                                            onclick=\"this.parentNode.querySelector('input[type=number]').stepUp()\">
                                        <i class=\"fas fa-plus text-danger\"></i>
                                        <i class=\"bi bi-plus\"></i>
                                    </button>
                                </div>
                                <div class=\"col-md-3 col-lg-2 col-xl-2 offset-lg-1\">
                                    <h5 class=\"mb-0\">";
                // line 54
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "products", [], "any", false, false, false, 54), "price", [], "any", false, false, false, 54), "html", null, true);
                echo "</h5>
                                </div>
                                <div class=\"col-md-1 col-lg-1 col-xl-1 text-end\">
                                    <a href=\"";
                // line 57
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cart_remove", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "products", [], "any", false, false, false, 57), "id", [], "any", false, false, false, 57)]), "html", null, true);
                echo "\" class=\"text-danger\">
                                        <i class=\"bi bi-trash\"></i>
                                    </a>
                                </div>

                            </div>
                        </div>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "
                    </div>
                    <div class=\"text-right\">
                        <h3 class=\"text-danger\">Total : ";
            // line 68
            echo twig_escape_filter($this->env, (isset($context["total"]) || array_key_exists("total", $context) ? $context["total"] : (function () { throw new RuntimeError('Variable "total" does not exist.', 68, $this->source); })()), "html", null, true);
            echo " €</h3>
                    </div>

                    <div class=\"card mb-4\">
                        <div class=\"card-body p-4 d-flex flex-row\">
                            <div class=\"form-outline flex-fill\">
                                <input type=\"text\" id=\"form1\" class=\"form-control form-control-lg\" />
                                <label class=\"form-label\" for=\"form1\">Discound code</label>
                            </div>
                            <button type=\"button\" class=\"btn btn-outline-primary     btn-lg ms-3\">Apply</button>
                        </div>
                    </div>

                    <div class=\"card\">
                        <div class=\"card-body\">
                            <button type=\"button\" class=\"btn btn-primary btn-block btn-lg\">Payment</button>
                        </div>
                    </div>

                </div>
                ";
        } else {
            // line 89
            echo "                    <h1 class=\"text-center\"> Your Cart is empty </h1>
                ";
        }
        // line 91
        echo "            </div>
        </div>
    </section>
        ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "cart/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 91,  210 => 89,  186 => 68,  181 => 65,  167 => 57,  161 => 54,  148 => 44,  136 => 35,  127 => 31,  121 => 27,  117 => 26,  106 => 18,  103 => 17,  94 => 8,  92 => 7,  88 => 5,  78 => 4,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block title %} la liste des products {% endblock %}

{% block body %}

    <section id=\"section\" class=\"my-5 mx-4 h-100\" id=\"section\">
        {% if items | length > 0 %}
        <div class=\"container h-100 py-5\">
            <div class=\"row d-flex justify-content-center align-items-center h-100\">
                <div class=\"col-8\">

                    <div class=\"d-flex justify-content-between align-items-center mb-4\">
                        <h3 class=\"fw-normal mb-0 text-black\">Shopping Cart</h3>
                        <div>
                           {# <p class=\"mb-0\"><span class=\"text-muted\">Sort by:</span> <a href=\"#!\" class=\"text-body\">price <i
                                            class=\"fas fa-angle-down mt-1\"></i></a></p>#}
                            <h3 class=\"\">
                                <a href=\"{{ path('app_home') }}\" class=\"text-decoration-none fw-normal mb-0 text-white btn btn-primary\">
                                     Add More Products to cart
                                </a>
                            </h3>
                        </div>
                    </div>

                    <div class=\"card rounded-3 mb-4\">
                        {% for item in items %}
                        <div class=\"card-body p-4\">
                            <div class=\"row d-flex justify-content-between align-items-center\">
                                <div class=\"col-md-2 col-lg-2 col-xl-2\">
                                    <img
                                            src=\"{{ asset('assets/uploads/Products/mini/300x300-' ~  item.products.images[0].name) }}\" alt=\"{{ item.products.images[0].name }}\"
                                            class=\"img-fluid rounded-3\" >
                                </div>
                                <div class=\"col-md-3 col-lg-3 col-xl-3\">
                                    <p class=\"lead fw-normal mb-2\">{{ item.products.name }}</p>
                                   <!-- <p><span class=\"text-muted\">Size: </span>M <span class=\"text-muted\">Color: </span>Grey</p>!-->
                                </div>
                                <div class=\"col-md-3 col-lg-3 col-xl-2 d-flex\">
                                    <button class=\"btn btn-link px-2\"
                                            onclick=\"this.parentNode.querySelector('input[type=number]').stepDown()\">
                                        <i class=\"bi bi-dash\"></i>
                                    </button>

                                    <input id=\"form1\" min=\"0\" name=\"quantity\" value=\"{{ item.quantity }}\" type=\"number\"
                                           class=\"form-control form-control-sm\" />

                                    <button class=\"btn btn-link px-2\"
                                            onclick=\"this.parentNode.querySelector('input[type=number]').stepUp()\">
                                        <i class=\"fas fa-plus text-danger\"></i>
                                        <i class=\"bi bi-plus\"></i>
                                    </button>
                                </div>
                                <div class=\"col-md-3 col-lg-2 col-xl-2 offset-lg-1\">
                                    <h5 class=\"mb-0\">{{ item.products.price }}</h5>
                                </div>
                                <div class=\"col-md-1 col-lg-1 col-xl-1 text-end\">
                                    <a href=\"{{ path('cart_remove', { id: item.products.id }) }}\" class=\"text-danger\">
                                        <i class=\"bi bi-trash\"></i>
                                    </a>
                                </div>

                            </div>
                        </div>
                        {% endfor %}

                    </div>
                    <div class=\"text-right\">
                        <h3 class=\"text-danger\">Total : {{  total }} €</h3>
                    </div>

                    <div class=\"card mb-4\">
                        <div class=\"card-body p-4 d-flex flex-row\">
                            <div class=\"form-outline flex-fill\">
                                <input type=\"text\" id=\"form1\" class=\"form-control form-control-lg\" />
                                <label class=\"form-label\" for=\"form1\">Discound code</label>
                            </div>
                            <button type=\"button\" class=\"btn btn-outline-primary     btn-lg ms-3\">Apply</button>
                        </div>
                    </div>

                    <div class=\"card\">
                        <div class=\"card-body\">
                            <button type=\"button\" class=\"btn btn-primary btn-block btn-lg\">Payment</button>
                        </div>
                    </div>

                </div>
                {% else %}
                    <h1 class=\"text-center\"> Your Cart is empty </h1>
                {% endif %}
            </div>
        </div>
    </section>
        {% endblock %}








        {#{% if items | length > 0 %}
         <table class=\"table\">
             <thead>
             <tr>
                 <td>Product</td>
                 <td>Price</td>
                 <td>Quantity</td>
                 <td>Total</td>
                 <td>Aktion</td>
             </tr>
             </thead>
             <tbody>
             {% for item in items %}

             <tr>
                 <td>{{ item.products.name }}</td>
                 <td>{{ item.products.price }}</td>
                 <td>{{ item.quantity }}</td>
                 <td>{{ item.products.price * item.quantity }}</td>
                 <td>
                     <a href=\"{{ path('cart_remove', { id: item.products.id }) }}\" class=\"btn btn-danger\">
                         surprimmer
                     </a>

                 </td>
             </tr>
             {% endfor %}
             </tbody>
             <tfoot>
               <tr>
                   <td colspan=\"3\" class=\"text-right\">Total :</td>
                   <td>{{  total }}</td>
                   <td></td>
               </tr>
             </tfoot>
         </table>

        {% else %}
            <h1 class=\"text-center\"> Your Cart is empty </h1>
        {% endif %}



    </section>


{% endblock %}

#}", "cart/index.html.twig", "/var/www/html/templates/cart/index.html.twig");
    }
}
