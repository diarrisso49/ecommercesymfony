<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _partials/_navbar.html.twig */
class __TwigTemplate_6c2bf10533da12fe1648c8fcf541561e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/_navbar.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_partials/_navbar.html.twig"));

        // line 1
        echo "
<!-- Navigation-->
<nav class=\"navbar navbar-expand-lg navbar-light bg-light navbar-static-top\">
    <div class=\"container px-4 px-lg-5\">
        <a class=\"navbar-brand \" href=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_home");
        echo "\">Diarrisso Shop</a>
        <button class=\"navbar-toggler\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"><span class=\"navbar-toggler-icon\"></span></button>
        <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
            <ul class=\"navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4 \">
                <li class=\"nav-item\"> <a class=\"nav-link  active\" href=\"";
        // line 9
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_home");
        echo "\">Home <span class=\"sr-only\">(current)</span></a></li>
                <li class=\"nav-item\"><a class=\"nav-link \" href=\"#!\">Category</a></li>
                <li class=\"nav-item\"><a class=\"nav-link \" href=\"#!\">Kontakt</a></li>
            </ul>

        </div>
        <ul class=\"navbar-nav mr-auto mb-2 mb-lg-0\">
            ";
        // line 16
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 16, $this->source); })()), "user", [], "any", false, false, false, 16)) {
            // line 17
            echo "
                ";
            // line 18
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_PRODUCTS_ADMIN")) {
                // line 19
                echo "                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"";
                // line 20
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_index");
                echo "\">Dashboard</a>
                    </li>
                ";
            }
            // line 23
            echo "
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"";
            // line 25
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile_index");
            echo "\">Profile</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"";
            // line 28
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo "\">Abmelden</a>
                </li>
                    <a class=\"btn btn-outline-dark\" href=\"";
            // line 30
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cart_panier");
            echo "\" >
                        <i class=\"bi-cart-fill me-1\"></i>
                        Cart
                        <span class=\"badge bg-dark ms-1 rounded-pill\">0</span>
                    </a>
            ";
        } else {
            // line 36
            echo "                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"";
            // line 37
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_login");
            echo "\">
                        Anmelden
                    </a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"";
            // line 42
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_register");
            echo "\">Registrieren</a>
                </li>
                <a class=\"btn btn-outline-dark\" href=\"";
            // line 44
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cart_panier");
            echo "\" >
                    <i class=\"bi-cart-fill me-1\"></i>
                    Cart
                    <span class=\"badge bg-dark ms-1 rounded-pill\">0</span>
                </a>

            ";
        }
        // line 51
        echo "        </ul>
    </div>
</nav>

";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "_partials/_navbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 51,  122 => 44,  117 => 42,  109 => 37,  106 => 36,  97 => 30,  92 => 28,  86 => 25,  82 => 23,  76 => 20,  73 => 19,  71 => 18,  68 => 17,  66 => 16,  56 => 9,  49 => 5,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
<!-- Navigation-->
<nav class=\"navbar navbar-expand-lg navbar-light bg-light navbar-static-top\">
    <div class=\"container px-4 px-lg-5\">
        <a class=\"navbar-brand \" href=\"{{ path('app_home') }}\">Diarrisso Shop</a>
        <button class=\"navbar-toggler\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"><span class=\"navbar-toggler-icon\"></span></button>
        <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
            <ul class=\"navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4 \">
                <li class=\"nav-item\"> <a class=\"nav-link  active\" href=\"{{ path('app_home') }}\">Home <span class=\"sr-only\">(current)</span></a></li>
                <li class=\"nav-item\"><a class=\"nav-link \" href=\"#!\">Category</a></li>
                <li class=\"nav-item\"><a class=\"nav-link \" href=\"#!\">Kontakt</a></li>
            </ul>

        </div>
        <ul class=\"navbar-nav mr-auto mb-2 mb-lg-0\">
            {%  if app.user %}

                {% if is_granted('ROLE_PRODUCTS_ADMIN') %}
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"{{ path('admin_index') }}\">Dashboard</a>
                    </li>
                {% endif %}

                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"{{ path('profile_index') }}\">Profile</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"{{ path('app_logout') }}\">Abmelden</a>
                </li>
                    <a class=\"btn btn-outline-dark\" href=\"{{ path('cart_panier') }}\" >
                        <i class=\"bi-cart-fill me-1\"></i>
                        Cart
                        <span class=\"badge bg-dark ms-1 rounded-pill\">0</span>
                    </a>
            {% else %}
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"{{ path('app_login') }}\">
                        Anmelden
                    </a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"{{ path('app_register') }}\">Registrieren</a>
                </li>
                <a class=\"btn btn-outline-dark\" href=\"{{ path('cart_panier') }}\" >
                    <i class=\"bi-cart-fill me-1\"></i>
                    Cart
                    <span class=\"badge bg-dark ms-1 rounded-pill\">0</span>
                </a>

            {% endif %}
        </ul>
    </div>
</nav>

", "_partials/_navbar.html.twig", "/var/www/html/templates/_partials/_navbar.html.twig");
    }
}
