<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/products/_form.html.twig */
class __TwigTemplate_2656e9351ad2e292dbfab07fbfbf3e25 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/products/_form.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/products/_form.html.twig"));

        // line 1
        echo "
";
        // line 2
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["ProductsForm"]) || array_key_exists("ProductsForm", $context) ? $context["ProductsForm"] : (function () { throw new RuntimeError('Variable "ProductsForm" does not exist.', 2, $this->source); })()), 'form_start');
        echo "
    ";
        // line 3
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["ProductsForm"]) || array_key_exists("ProductsForm", $context) ? $context["ProductsForm"] : (function () { throw new RuntimeError('Variable "ProductsForm" does not exist.', 3, $this->source); })()), "name", [], "any", false, false, false, 3), 'row');
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["ProductsForm"]) || array_key_exists("ProductsForm", $context) ? $context["ProductsForm"] : (function () { throw new RuntimeError('Variable "ProductsForm" does not exist.', 4, $this->source); })()), "description", [], "any", false, false, false, 4), 'row');
        echo "
    ";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["ProductsForm"]) || array_key_exists("ProductsForm", $context) ? $context["ProductsForm"] : (function () { throw new RuntimeError('Variable "ProductsForm" does not exist.', 5, $this->source); })()), "price", [], "any", false, false, false, 5), 'row');
        echo "
    ";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["ProductsForm"]) || array_key_exists("ProductsForm", $context) ? $context["ProductsForm"] : (function () { throw new RuntimeError('Variable "ProductsForm" does not exist.', 6, $this->source); })()), "categories", [], "any", false, false, false, 6), 'row');
        echo "
    ";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["ProductsForm"]) || array_key_exists("ProductsForm", $context) ? $context["ProductsForm"] : (function () { throw new RuntimeError('Variable "ProductsForm" does not exist.', 7, $this->source); })()), "stock", [], "any", false, false, false, 7), 'row');
        echo "



 ";
        // line 12
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 12, $this->source); })()), "request", [], "any", false, false, false, 12), "attributes", [], "any", false, false, false, 12), "get", [0 => "_route"], "method", false, false, false, 12) == "admin_products_edit")) {
            // line 13
            echo "
    <h2>images</h2>
    ";
            // line 15
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["products"]) || array_key_exists("products", $context) ? $context["products"] : (function () { throw new RuntimeError('Variable "products" does not exist.', 15, $this->source); })()), "images", [], "any", false, false, false, 15));
            foreach ($context['_seq'] as $context["_key"] => $context["images"]) {
                // line 16
                echo "
        <div>
            <img src=\"";
                // line 18
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("assets/uploads/Products/mini/300x300-" . twig_get_attribute($this->env, $this->source, $context["images"], "name", [], "any", false, false, false, 18))), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["images"], "name", [], "any", false, false, false, 18), "html", null, true);
                echo "\" width=\"150\">
            <a href=\"";
                // line 19
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_products_delete_image", ["id" => twig_get_attribute($this->env, $this->source, $context["images"], "id", [], "any", false, false, false, 19)]), "html", null, true);
                echo "\" data-delete data-token=\"";
                echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(("delete" . twig_get_attribute($this->env, $this->source, $context["images"], "id", [], "any", false, false, false, 19))), "html", null, true);
                echo "\">supprimmer</a>
        </div>

    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['images'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "
";
        }
        // line 25
        echo "
 ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["ProductsForm"]) || array_key_exists("ProductsForm", $context) ? $context["ProductsForm"] : (function () { throw new RuntimeError('Variable "ProductsForm" does not exist.', 26, $this->source); })()), "images", [], "any", false, false, false, 26), 'row');
        echo "

    <button type=\"submit\" class=\"btn btn-primary\"> ";
        // line 28
        echo twig_escape_filter($this->env, ((array_key_exists("button_label", $context)) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 28, $this->source); })()), "Add")) : ("Add")), "html", null, true);
        echo "</button>
";
        // line 29
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["ProductsForm"]) || array_key_exists("ProductsForm", $context) ? $context["ProductsForm"] : (function () { throw new RuntimeError('Variable "ProductsForm" does not exist.', 29, $this->source); })()), 'form_end');
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/products/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 29,  117 => 28,  112 => 26,  109 => 25,  105 => 23,  93 => 19,  87 => 18,  83 => 16,  79 => 15,  75 => 13,  73 => 12,  66 => 7,  62 => 6,  58 => 5,  54 => 4,  50 => 3,  46 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
{{ form_start( ProductsForm )}}
    {{ form_row( ProductsForm.name )}}
    {{ form_row( ProductsForm.description)}}
    {{ form_row( ProductsForm.price )}}
    {{ form_row( ProductsForm.categories)}}
    {{ form_row( ProductsForm.stock )}}



 {# on affiche les images si on est en editio  #}
{% if app.request.attributes.get('_route') == 'admin_products_edit' %}

    <h2>images</h2>
    {% for images in products.images %}

        <div>
            <img src=\"{{ asset('assets/uploads/Products/mini/300x300-' ~ images.name) }}\" alt=\"{{ images.name }}\" width=\"150\">
            <a href=\"{{ path('admin_products_delete_image', {id: images.id}) }}\" data-delete data-token=\"{{ csrf_token('delete' ~ images.id) }}\">supprimmer</a>
        </div>

    {% endfor %}

{% endif %}

 {{ form_row( ProductsForm.images )}}

    <button type=\"submit\" class=\"btn btn-primary\"> {{ button_label| default('Add') }}</button>
{{ form_end( ProductsForm )}}", "admin/products/_form.html.twig", "/var/www/html/templates/admin/products/_form.html.twig");
    }
}
